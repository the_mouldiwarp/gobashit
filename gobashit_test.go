package gobashit

import "testing"

func TestSplitCommandString(t *testing.T) {
	e := []string{"space", "separated", "command", "and", "arguments"}
	r := splitCommandString("space separated command and arguments")
	for i := range e {
		if e[i] != r[i] {
			t.Error("Expected: ", e[i], " Got: ", r[i])
		}
	}
}

func TestSplitComArgs(t *testing.T) {
	ea := []string{"separated", "command", "and", "arguments"}
	ec := "space"
	rc, ra := splitComArgs("space separated command and arguments")
	for i := range ea {
		if ea[i] != ra[i] {
			t.Error("Expected: ", ea[i], " Got: ", ra[i])
		}
	}
	if ec != rc {
		t.Error("Expected: ", ec, " Got: ", rc)
	}
}

func TestOutToStr(t *testing.T) {
	e := "banana"
	r := outToStr([]byte("banana"))
	if e != r {
		t.Error("Expected: ", e, " Got: ", r)
	}
}

func TestStrSplitNewLine(t *testing.T) {
	e := []string{"test", "string"}
	r := strSplitNewLine("test\nstring")
	for i := range r {
		if e[i] != r[i] {
			t.Error("Expected: ", e[i], " Got: ", r[i])
		}
	}
}
