// Package gobashit is a library that executes a command (with arguments) returning
// a slice of data representing line by line output from the command.
// Intended to work with BASH.
package gobashit

import (
	"fmt"
	"os"
	"os/exec"
	"strings"
)

// splitCommandString takes the command line as a string and breaks it into an array.
func splitCommandString(s string) []string {
	r := strings.Split(s, " ")
	return r
}

// SplitComArgs Takes a command string (wit and arguments) and returns a string
// representing the executable and an array representing the arguments.
func splitComArgs(s string) (string, []string) {
	i := splitCommandString(s)
	return i[0], i[1:]
}

// outToStr takes a byte array and converts it to a string
func outToStr(b []byte) string {
	return string(b[:len(b)])
}

// strSplitNewline takes a string and creates a string slice, splitting on newline
func strSplitNewLine(s string) []string {
	return strings.Split(s, "\n")
}

// ExecuteCmd takes a command and arguments as a string and returns a slice of data
// representing the command output
func ExecuteCmd(s string) []string {
	c, a := splitComArgs(s)

	var (
		co []byte
		e  error
	)
	if co, e = exec.Command(c, a...).Output(); e != nil {
		fmt.Fprintln(os.Stderr, "There was an error running the command: ", e)
		os.Exit(1)
	}
	return strSplitNewLine(outToStr(co))
}
